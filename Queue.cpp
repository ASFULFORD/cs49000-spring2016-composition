//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================

//
// Queue
//
// Default Constructor. last_ represents the last element of the queue and count_
// represents the number of elements in the queue.
template <typename T>
Queue <T>::Queue (void)
// COMMENT Initialize each variable on its own line.
// *** FIX ***
// Edited so that each variable is on its own line.  I understand this will help with debugging.
: queue_array_(),
  first_(0),
  last_(-1),
  count_(0) 
  { }

//
// Queue
//
// Copy Constructor. Deep copy.
// *** HELP ***
// Whenever I try to access the Array's data_ by using [].  I get an error 
// saying that data_ is private.  However, the 'T get (size_t index) const;'
// method works just fine.  Also, the implementation of get() and [] are
// exactly the same.
template <typename T>
Queue <T>::Queue (const Queue & queue)
// COMMENT Initialize each variable on its own line.
// *** FIX ***
// Edited so that each variable is on its own line.  I understand this will help with debugging.
: queue_array_(queue.queue_array_),
  first_(queue.first_),
  last_(queue.last_), 
  count_(queue.count_)
{ }

//
// ~Queue
//
// Deconstructor.  Empty since I don't have any pointers or use the keyword new.
template <typename T>
Queue <T>::~Queue (void) { }

//
// enqueue
//
// Use an if statement to check whether or not the end of the queue is in bounds.
// If the queue is full, resize the Array then enqueue.
// *** FIX ***
// Enqueue is changed so that it now first checks if the queue is full or not.  It
// it is full, the method will resize the array and sort the elements so that the
// 0 index is the beginning of the queue.  
// If the array is not full, it checks whether or not the last_ index is at the end
// of the array.  If it is, then it will loop around to the beginning of the array
// and populate those elements.
template <typename T>
void Queue <T>::enqueue (T element)
{
	if(this->count_ < this->queue_array_.Array_Base<T>::size())
	{	
		if(this->last_ == this->queue_array_.Array_Base<T>::size() - 1)
		{
			this->last_ = 0;
			this->count_++;
			this->queue_array_.Array_Base<T>::set(this->last_, element);
		}
		else
		{
			this->last_++;
			this->count_++;
			this->queue_array_.Array_Base<T>::set(this->last_, element);
		}
	}
	else
	{
		this->queue_array_.Array<T>::resize(this->queue_array_.Array<T>::max_size() * 2);
 		
 		if(this->first_ != 0)
 		{
 			T *tmp = new T[this->count_ * 2];
 			size_t this_iter = 0;
 			size_t tmp_iter = 0;

 			for(tmp_iter = 0, this_iter = first_; tmp_iter < (this->count_ - this->first_); ++tmp_iter, ++this_iter)
 			{
 				tmp[tmp_iter] = this->queue_array_.Array_Base<T>::get(this_iter);
 			}
 			
 			for(this_iter = 0; this_iter < this->first_; ++tmp_iter, ++this_iter)
 			{
 				tmp[tmp_iter] = this->queue_array_.Array_Base<T>::get(this_iter);
 			}

 			for(size_t i = 0; i < this->queue_array_.Array_Base<T>::size(); ++i)
 			{
 				this->queue_array_.Array_Base<T>::set(i, tmp[i]);
 			}

 			this->first_ = 0;
 			this->last_ = count_ - 1;
 			this->last_++;
 			this->count_++;
 			this->queue_array_.Array_Base<T>::set(this->last_, element);
 			delete[] tmp;
 		}
 		else
 		{
 			this->last_++;
 			this->count_++;
 			this->queue_array_.Array_Base<T>::set(last_, element);
 		}
	}
}

//
// dequeue
//
// If the queue is empty, thow an exception.  If not, store head of
// queue in a variable, shift everything in the array down and return
// the value stored in the variable.
template <typename T>
T Queue <T>::dequeue (void)
{
	if(this->count_ == 0)
		throw empty_exception();
	
	T ret_val = this->queue_array_.Array_Base<T>::get(first_);
  
  	// COMMENT This design is OK, but it is not the best design. This will be
  	// a very expensive array to use if you are dequeing a lot of elements. This
  	// is because you are copying N elements each time you dequeue 1 element.
  	// Instead, you only want to copy element when necessary. Come up with a better
  	// design that is not as expensive for the client to use.
	// *** FIX ***
	// Dequeue no longer shifts the array to the left.  Sorting takes place in the
	// enqueue method only when resizing takes place.  See comments for enqueue.
	
	this->first_++;
	this->count_--;

	return ret_val;
}

//
// operator =
//
// Same implementation of the copy constructor.  Deep copy.
template <typename T>
const Queue <T> & Queue <T>::operator = (const Queue & rhs)
{
  	// COMMENT Always check for self-assignment first before continuing.
	// *** FIX *** Check for self-assignment first.
  	if(this == rhs)
  		return *this;
  		 
	this->last_ = rhs.last_;
	this->count_ = rhs.count_;

	if(rhs.size() > 0)
	{
		for(size_t i = 0; i < rhs.size(); ++i)
			this->set(i, rhs.data_[i]);
	}
	return *this;
}

//
// clear
//
// Simply set the index to the end of the queue to -1 and count_ to 0.
// This represents an empty stack.
template <typename T>
void Queue <T>::clear (void)
{
	this->last_ = -1;
	this->count_ = 0;
}
