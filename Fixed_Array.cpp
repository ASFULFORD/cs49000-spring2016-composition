// $Id: Fixed_Array.cpp 827 2011-02-07 14:20:53Z hillj $

//==============================================================================
/**
 * Honor Pledge:
 *
 * I pledge that I have neither given nor received any help
 * on this assignment.
 */
//==============================================================================

//
// Fixed_Array
//
// Default Constructor.  Initialized an array with the size of N.
// *** HELP ***
// I am not completely sure if this is the intended implementation.
template <typename T, size_t N>
Fixed_Array <T, N>::Fixed_Array (void)
// COMMENT You should call the appropriate base class contructor here.
// *** FIX ***
// Called the appropriate base class constructor.
: Array_Base <T>(N)
{ }

//
// Fixed_Array
//
// Copy Constructor. Deep Copy
// *** HELP ***
// Whenever I try to access the Array's data_ by using [].  I get an error 
// saying that data_ is private.  However, the 'T get (size_t index) const;'
// method works just fine.  Also, the implementation of get() and [] are
// exactly the same.
template <typename T, size_t N>
Fixed_Array <T, N>::Fixed_Array (const Fixed_Array <T, N> & arr)
// COMMENT You should call the appropriate base class contructor here.
// *** FIX ***
// Called the appropriate base class constructor.
: Array_Base <T>(N)
{
	*this = arr;
}

//
// Fixed_Array
//
// Fill Constructor.
template <typename T, size_t N>
Fixed_Array <T, N>::Fixed_Array (T fill)
// COMMENT You should call the appropriate base class contructor here.
// *** FIX ***
// Called the appropriate base class constructor.
: Array_Base <T>(N, fill)
{ }

//
// ~Fixed_Array
//
// Deconstructor.  Empty since I don't have any pointers or use the keyword
// new.
template <typename T, size_t N>
Fixed_Array <T, N>::~Fixed_Array (void) { }

//
// operator =
//
// Same implementation of the copy constructor.  Deep copy.
template <typename T, size_t N>
const Fixed_Array <T, N> & Fixed_Array <T, N>::operator = (const Fixed_Array <T, N> & rhs)
{
  if(this == &rhs)
    return *this;

  this->Array_Base<T>::cur_size_ = rhs.size();
  
  for(size_t i = 0; i < rhs.size(); ++i)
    this->Array_Base<T>::set(i, rhs.get(i));
  return *this;
}